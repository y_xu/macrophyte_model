from django.core.management.base import BaseCommand, CommandError
from macrophyte.models import Plant, Leaf, Algae, Section, Light, Experiment
import csv
from datetime import datetime
from pytz import timezone

class Command(BaseCommand):
    help = 'cal section data'

    def handle(self, *args, **options):
        #light = Light.objects.all()
        #with open("log/section.csv", "w") as myfile:
        #         myfile.write('id,current_time,surface_light,time_depth,water_depth, water_attenuation_pct,algae_biomass,algae_attenuation_pct,time_available_light,gross_photo_rate,net_photo,sum_net_photo\n')
        
        for experiment in Experiment.objects.all().order_by('-id'):
            light = Light.objects.filter(experiment=experiment)
            for plant in Plant.objects.filter(experiment=experiment).order_by('id'):
                #for leaf in Leaf.objects.filter(leaf_rank=5).order_by('-leaf_rank'):
                for leaf in Leaf.objects.filter(plant=plant).order_by('-leaf_rank'):
                    for rank in xrange(leaf.section_num):
                        section_rank = rank + 1
                        section, created = Section.objects.get_or_create(leaf=leaf, section_rank=section_rank)
                        if created or section.net_photo==None:
                           section.light=light
                           section.save()
                        
                        print "%s net_photo: %s"%(section, section.net_photo)
                    leaf.setNetPhoto()
                    leaf.save()
                    print "%s saved"%leaf
                #exit()
                plant.setNetPhoto()
                plant.save()
                print "%s saved"%plant
                #exit()
        
        
        
        

        
        '''
        for plant in Plant.objects.all().order_by('id'):
            for leaf in Leaf.objects.filter(plant=plant).order_by('-leaf_rank'):
                for rank in xrange(leaf.section_num):
                    section_rank = rank + 1
                    section = Section.objects.create(leaf=leaf, section_rank=section_rank)
                    section.light=light
                    section.save()
                    print "%s net_photo: %s"%(section, section.net_photo)
                leaf.setNetPhoto()
                leaf.save()
                print "%s saved"%leaf
            plant.setNetPhoto()
            plant.save()
            print "%s saved"%plant
            break
        '''
