from django.core.management.base import BaseCommand, CommandError
from macrophyte.models import Plant, Leaf, Algae, Experiment
import csv
from datetime import datetime
from pytz import timezone

class Command(BaseCommand):
    help = 'Import data'

    def handle(self, *args, **options):
        print "OK"
        with open('data/Validate/1PlantBiomass.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            experiment, created = Experiment.objects.get_or_create(id=0)
            plant = Plant.objects.create(experiment=experiment)
            for row in reader:
                
                algae = Algae.objects.create(algae_biomass=row['epi_dw'])
                leaf = Leaf.objects.create(plant=plant, 
                                        algae=algae,
                                        leaf_rank=row['leaf_rank'],
                                        age=row['leaf_age'],
                                        length=row['leaf_length'],
                                        width=row['leaf_width'],
                                        real_growth = row['nb_dw'])
                plant.real_growth = float(plant.real_growth)+float(leaf.real_growth)
                experiment, created = Experiment.objects.get_or_create(id=row['experiment_id'])
                plant.experiment = experiment
                plant.save()
                if row['leaf_rank'] == '1' or row['leaf_rank'] == 1:
                    experiment, created = Experiment.objects.get_or_create(id=0)
                    plant = Plant.objects.create(experiment=experiment)
                print "Save:"
                #print(row['leaf_rank'], row['leaf_age'], row['leaf_length'])
            plant.delete()
        experiment, created = Experiment.objects.get_or_create(id=0)
        experiment.delete()