from django.core.management.base import BaseCommand, CommandError
from macrophyte.models import Water, Light, Experiment
import csv
from datetime import datetime
from django.utils import timezone
import pytz
from dateutil.tz import tzlocal

localtimezone = tzlocal()


class Command(BaseCommand):
    help = 'Import data'

    def handle(self, *args, **options):
        print "OK"
        with open('data/Validate/ENV.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                experiment, created = Experiment.objects.get_or_create(id=row['experiment_id'])
                
                #date_and_time = datetime.strptime(row['time'], '%m/%d/%Y %H:%M').replace(tzinfo=localtimezone)
                date_and_time = datetime.strptime(row['time'], '%b %d %Y  %I:%M %p').replace(tzinfo=localtimezone)

                water = Water.objects.create(depth=float(row['water_level']))
                light, created = Light.objects.get_or_create(experiment=experiment,
                                                            surface_light=row['field_light'],
                                                            date_and_time=date_and_time,
                                                            water=water)
                print date_and_time, row['water_level'], row['field_light']