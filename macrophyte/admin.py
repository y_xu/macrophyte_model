# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from macrophyte.models import *
from macrophyte.actions import *


admin.site.register(Water)
admin.site.register(Algae)

class SettingAdmin(admin.ModelAdmin):
    actions = [import_experiment_setting]

admin.site.register(Setting, SettingAdmin)

class ExperimentAdmin(admin.ModelAdmin):
    list_display = ('__unicode__','name','ql','kd','pmax','km','respiration','ic','light_sat','epi_light_coef','epi_bio_loss_coef','algae_biomass')
    actions = [import_selected_experiments, run_selected_experiments, continue_selected_experiments, prepare_section_csv_selected_experiments]
    list_editable = ('ql','kd','pmax','km','respiration','ic','light_sat','epi_light_coef','epi_bio_loss_coef','algae_biomass')
admin.site.register(Experiment, ExperimentAdmin)

class LightAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'date_and_time', 'water', 'station_light','surface_light')
admin.site.register(Light, LightAdmin)


class PlantAdmin(admin.ModelAdmin):
    list_display = ('__unicode__','real_growth','predicted_growth', 'gross_photo_product', 'respiration','net_photo', 'new_leaf')
    actions = [download_section_total_csv]
    ordering = ('experiment','id',)
admin.site.register(Plant, PlantAdmin)

class LeafAdmin(admin.ModelAdmin):
    list_display = ('__unicode__','plant','age','length','width','section_num','algae','algae_biomass_ratio','real_growth','gross_photo_product','respiration','net_photo','self_used_net_photo')
    list_filter = ('plant__experiment', )
    ordering = ('id',)
admin.site.register(Leaf, LeafAdmin)

class SectionAdmin(admin.ModelAdmin):
    list_display = ('__unicode__','gross_photo','respiration','net_photo')
    actions = [download_section_csv]
    list_filter = ('leaf__plant', 'section_rank',)
admin.site.register(Section, SectionAdmin)
