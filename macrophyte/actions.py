from macrophyte import tasks
from macrophyte.models import Section
from django.contrib import messages
from datetime import datetime
from django.db.models import Max
from django.db.models import F, FloatField, Sum

def import_experiment_setting(modeladmin, request, queryset):
    for q in queryset:
        tasks.import_experiment_from_setting.delay(q)
    messages.add_message(request, messages.INFO, "Import Experiment(s) from Setting")

def import_selected_experiments(modeladmin, request, queryset):
    
    for q in queryset:
        tasks.import_data.delay(q)

    messages.add_message(request, messages.INFO, "Import Experiment Data")
    
def run_selected_experiments(modeladmin, request, queryset):
    for q in queryset:
        tasks.run_data.delay(q)
    messages.add_message(request, messages.INFO, "Run Experiment Data")
    
def continue_selected_experiments(modeladmin, request, queryset):
    for q in queryset:
        tasks.continue_run_data.delay(q)
    messages.add_message(request, messages.INFO, "Continue run Experiment Data")
    
def prepare_section_csv_selected_experiments(modeladmin, request, queryset):
    for q in queryset:
        tasks.section_csv_download.delay(q)
    messages.add_message(request, messages.INFO, "Prepare Section Download Data")
    
def download_section_csv(self, request, queryset):
    import csv
    from django.http import HttpResponse
    file_name = 'download/%s_%s.csv'%('section', datetime.now().strftime("%Y-%m-%dT%H%M%S"))
    f = open(file_name, 'wb')
    writer = csv.writer(f)
    writer.writerow(['id','gross_photo','respiration','net_photo'])
 
    for section in queryset:
        name = "Plant %s: Leaf: %s Section: %s" % (section.leaf.plant.id, section.leaf.leaf_rank, section.section_rank)
        writer.writerow([name, section.gross_photo, section.respiration, section.net_photo])
 
    f.close()
 
    f = open(file_name, 'r')
    response = HttpResponse(f, content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=%s_%s.csv'%('section', datetime.now().strftime("%Y-%m-%dT%H%M%S"))
    return response
    
def download_section_total_csv(self, request, queryset):
    import csv
    from django.http import HttpResponse
    file_name = 'download/%s_%s.csv'%('section_total', datetime.now().strftime("%Y-%m-%dT%H%M%S"))
    f = open(file_name, 'wb')
    writer = csv.writer(f)
    writer.writerow(['id','gross_photo','respiration','net_photo'])
    
    for plant in queryset:
        for rank in xrange(Section.objects.filter(leaf__plant= plant).aggregate(Max('section_rank'))['section_rank__max']):
            section_rank = rank + 1
            gross_photo = Section.objects.filter(leaf__plant=plant, section_rank=section_rank).aggregate(sum_gross_photo=Sum(F('gross_photo'), output_field=FloatField()))
            respiration = Section.objects.filter(leaf__plant=plant, section_rank=section_rank).aggregate(sum_respiration=Sum(F('respiration'), output_field=FloatField()))
            net_photo = Section.objects.filter(leaf__plant=plant, section_rank=section_rank).aggregate(sum_net_photo=Sum(F('net_photo'), output_field=FloatField()))
            name = 'Plant: %s Section: %s'%(plant.id, section_rank)
            writer.writerow([name, gross_photo['sum_gross_photo'], respiration['sum_respiration'], net_photo['sum_net_photo']])
 
    f.close()
 
    f = open(file_name, 'r')
    response = HttpResponse(f, content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=%s_%s.csv'%('section', datetime.now().strftime("%Y-%m-%dT%H%M%S"))
    return response
    
    
    
    
    
