# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from macrophyte.models import Leaf
from django.db.models import F, FloatField, Sum

# Create your models here.
class Plant(models.Model):
    experiment = models.ForeignKey('Experiment', on_delete=models.CASCADE)
    predicted_growth = models.FloatField(null=True)
    gross_photo_product = models.FloatField(null=True)
    respiration = models.FloatField(null=True)
    real_growth = models.FloatField(null=True, default=0)
    net_photo = models.FloatField(null=True)
    new_leaf = models.FloatField(null=True)
    
    #＃predicted growth = growth rate* plant net photo
    #predicted growth vs. real growth (real photo product)
    #@property
    #def setTide(self):
    #    return 1
    def setNewLeaf(self):
        self.new_leaf = 0
        for leaf in Leaf.objects.filter(plant=self):
            if leaf.self_used_net_photo == 0:
                pass
            elif leaf.age <= 3:
                self.new_leaf = self.new_leaf + leaf.net_photo
            else:
                self.new_leaf = self.new_leaf + (leaf.net_photo - leaf.self_used_net_photo)
        return self.new_leaf
    
    def setNetPhoto(self):
        result = Leaf.objects.filter(plant=self, net_photo__gte=0).aggregate(sum_net_photo=Sum(F('net_photo'), output_field=FloatField()))
        self.net_photo = result['sum_net_photo']
        if self.net_photo == None:
           self.net_photo = 0
        return self.net_photo
    
    def setGrossPhotoProduct(self):
        result = Leaf.objects.filter(plant=self).aggregate(sum_gross_photo_product=Sum(F('gross_photo_product'), output_field=FloatField()))
        self.gross_photo_product = result['sum_gross_photo_product']
        return self.gross_photo_product
    
    def setRespiration(self):
        result = Leaf.objects.filter(plant=self).aggregate(sum_respiration=Sum(F('respiration'), output_field=FloatField()))
        self.respiration = result['sum_respiration']
        return self.respiration
    
    def setPredictedGrowth(self): #predicted_Growth
        #L_FRACTION = self.experiment.l_fraction
        
        self.predicted_growth = self.net_photo #* L_FRACTION # Ftaction of net photo allocated to leaves
        return self.predicted_growth
    
    def save(self, *args, **kwargs):
        super(Plant, self).save(*args, **kwargs)
    
    class Meta:
        ordering = ['-experiment','id']
    
    def __unicode__(self):
        return "Experiment: %s Plant: %s" % (self.experiment.id, self.id)
    