# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

# Create your models here.
class Setting(models.Model):
    name = models.CharField(max_length=50)
    biomass_file = models.FileField(upload_to='biomass/', blank=True, help_text="csv file only")
    env_file = models.FileField(upload_to='env/', blank=True, help_text="csv_file_only")
    setting_file = models.FileField(upload_to='setting/', blank=True, help_text="ql, kd, pmax, km, respiration, ic, light_sat, epi_light_coef, epi_bio_loss_coef, algae_biomass")

    def __unicode__(self):
        return "%s" % self.id
    
