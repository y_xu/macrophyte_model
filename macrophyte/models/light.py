# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from math import exp, log
# Create your models here.
class Light(models.Model):
    water = models.OneToOneField('Water', on_delete=models.CASCADE, null=True)
    experiment = models.ForeignKey('Experiment', on_delete=models.CASCADE)
    date_and_time = models.DateTimeField()
    station_light = models.FloatField(null=True)
    surface_light = models.FloatField(null=True)

    
    def getCalSurfaceLight(self):
        return 1
    
    def save(self, *args, **kwargs):
        super(Light, self).save(*args, **kwargs)
    
    def __unicode__(self):
        return "Light %s" % self.id
    