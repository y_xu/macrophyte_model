# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from macrophyte.models import Section
from django.db.models import F, FloatField, Sum

# Create your models here.
class Leaf(models.Model):
    plant = models.ForeignKey('Plant', on_delete=models.CASCADE)
    algae = models.OneToOneField('Algae', on_delete=models.CASCADE, null=True)
    leaf_rank = models.IntegerField()
    age = models.IntegerField(null=True)
    length = models.FloatField(null=True)
    width = models.FloatField(null=True)
    section_num = models.IntegerField(null=True)
    algae_biomass_ratio = models.FloatField(null=True)
    #algae_density = models.FloatField(null=True)
    real_growth= models.FloatField(null=True)
    net_photo = models.FloatField(null=True)
    self_used_net_photo = models.FloatField(null=True)
    gross_photo_product = models.FloatField(null=True)
    respiration = models.FloatField(null=True)
    #r = models.FloatField(null=True)
    
    def setCalSectionNum(self):
        if int(float(self.length)) == float(self.length):
            self.section_num = int(float(self.length))
        else:
            self.section_num = int(float(self.length)) + 1
        return self.section_num
    
    
    # self.algae_biomass_ration=float(self.algae.algae_biomass)/(float(self.width)*(section_num-7)(section_num-7+1)/2)
    #(epi-DW mg)/(1*W)*(n*(n+1)/2)
    #def setCalAlgaeDensity(self):
    #    self.algae_density= float(self.algae.algae_biomass)/(float(self.length)*float(self.width))    #(mg/cm^2)
    #    return self.algae_density
    
    def setCalAlgaeBiomassRatio(self):
        
        if int(self.section_num) > 7:
            #self.algae_biomass_ratio =(float(self.algae.algae_biomass)*float(self.width)*float(self.length))/(float(self.width)*(self.section_num-7)*(self.section_num-7+1)/2)
            self.algae_biomass_ratio =float(self.algae.algae_biomass)/(float(self.width)*(self.section_num-7)*(self.section_num-7+1)/2)
            #print "%s = float(%s)/(float(%s)*(%s-7)*(%s-7+1)/2)"%(self.algae_biomass_ratio, self.algae.algae_biomass, self.width, self.section_num, self.section_num)
        else:
            self.algae_biomass_ratio = 0
        return self.algae_biomass_ratio
    
    def setNetPhoto(self):
        result = Section.objects.filter(leaf=self).aggregate(sum_net_photo=Sum(F('net_photo'), output_field=FloatField()))
        self.net_photo = result['sum_net_photo']
        if self.net_photo == None:
            self.net_photo = 0
        #self.r = result['sum_net_photo']-self.real_growth
        
        #print "R^2 for leaf %s is %s"%(self.leaf_rank, r)

        return self.net_photo
        # def R=abs(sum net photo-nb_dw) return R
        #   each leaf has one R^2=R*R  
        #   I want to know the sum of all leaves R^2 and return the value   
        
    def setGrossPhotoProduct(self):
        result = Section.objects.filter(leaf=self).aggregate(sum_gross_photo_product=Sum(F('gross_photo'), output_field=FloatField()))
        self.gross_photo_product = result['sum_gross_photo_product']
        if self.gross_photo_product == None:
            self.gross_photo_product = 0
        return self.gross_photo_product
        
    def setRespiration(self):
        result = Section.objects.filter(leaf=self).aggregate(sum_respiration=Sum(F('respiration'), output_field=FloatField()))
        self.respiration = result['sum_respiration']
        if self.respiration == None:
            self.respiration = 0
        return self.respiration
    
    def setSelfUsedNetPhoto(self):
        if self.net_photo < 0:
            self.self_used_net_photo = 0
            return self.self_used_net_photo
        
        if self.age <= 3:
            self.self_used_net_photo = self.net_photo
        elif 3 < self.age <= 9:
            if self.net_photo > 52.3:
                self.self_used_net_photo = 52.3
            else:
                self.self_used_net_photo = self.net_photo
                
        elif 9 < self.age <= 15:
            if self.net_photo > 56.16:
                self.self_used_net_photo = 56.16
            else:
                self.self_used_net_photo = self.net_photo
        
        elif 15 < self.age <= 21:
            if self.net_photo > 40.56:
                self.self_used_net_photo = 40.56
            else:
                self.self_used_net_photo = self.net_photo
                
        elif 21 < self.age <= 27:
            if self.net_photo > 16.55:
                self.self_used_net_photo = 16.55
            else:
                self.self_used_net_photo = self.net_photo
         
        elif 27 < self.age <= 33:
            if self.net_photo > 11.70:
                self.self_used_net_photo = 11.70
            else:
                self.self_used_net_photo = self.net_photo 
                
        elif self.age > 33:
             self.self_used_net_photo = 0
        return self.self_used_net_photo
    
        #return self.LeaveIncreasePerShoot
    #@property
    #def setTide(self):
    #    return 1
    
    
    def save(self, *args, **kwargs):
        if self.algae != None:
            self.setCalSectionNum()
            self.setCalAlgaeBiomassRatio()
            #self.setCalAlgaeDensity()
            
        super(Leaf, self).save(*args, **kwargs)
    
    def __unicode__(self):
        return "Plant %s Leaf %s" % (self.plant.id, self.leaf_rank)
    
    #https://macrophyte-model-yangx.c9users.io/admin