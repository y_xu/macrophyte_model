# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

# Create your models here.
class Algae(models.Model):
    algae_biomass = models.FloatField()
    
    def save(self, *args, **kwargs):
        super(Algae, self).save(*args, **kwargs)
    
    def __unicode__(self):
        return "%s" % self.algae_biomass
    