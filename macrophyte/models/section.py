# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from math import exp, pow
from django.db import models
from django.utils import timezone


# Create your models here.
class Section(models.Model):
    leaf = models.ForeignKey('Leaf', on_delete=models.CASCADE)
    light = models.ManyToManyField('Light', null=True)
    section_rank = models.IntegerField()
    gross_photo = models.FloatField(null=True)
    respiration = models.FloatField(null=True)
    net_photo = models.FloatField(null=True)
    #loss_by_epi = models.FloatField(null=True) 
    
    
    def setCalNetPhoto(self):
        sum_gross_photo =0
        sum_respiration =0
        sum_net_photo = 0
        #sum_loss_by_epi = 0
        QL = self.leaf.plant.experiment.ql #1
        KD = self.leaf.plant.experiment.kd #1.56 #(1/m)
        PMAX = self.leaf.plant.experiment.pmax #0.077 mg CO2 m-2s-1
        IC = self.leaf.plant.experiment.ic #9.4 #(uE/s/m^2)
        LIGHT_SAT = self.leaf.plant.experiment.light_sat #846.15 #(uE/s/m^2)
        KM =self.leaf.plant.experiment.km #123.08 #(uE/s/m^2)
        EPI_LIGHT_COEF = self.leaf.plant.experiment.epi_light_coef #1
        EPI_BIO_LOSS_COEF = self.leaf.plant.experiment.epi_bio_loss_coef #1
        RESPIRATION = self.leaf.plant.experiment.respiration # 2.57mg CO2/(m2*15min)
        ALGAE_BIOMASS = self.leaf.plant.experiment.algae_biomass
        
        
        
        for time in self.light.all().order_by('date_and_time'):
            time_depth = (time.water.depth - self.section_rank + 0.5)/100  #input depth(cm), after/100, (m)
            if time_depth<0:
                time_depth=0
            water_attenuation_pct=exp(-1*KD*time_depth)
            
            
            if ALGAE_BIOMASS == None:
                if self.section_rank>7:
                    algae_biomass= self.leaf.algae_biomass_ratio*(self.section_rank-7)*(float(self.leaf.width))
                    
                    algae_attenuation_pct = (EPI_LIGHT_COEF*0.8)*exp(-1*algae_biomass/(EPI_LIGHT_COEF*3.63)) #0.8 #0.3*exp(-1*algae_biomass)
                else:
                    algae_biomass=0
                    algae_attenuation_pct =1
            else:
                #algae_biomass = ALGAE_BIOMASS
                algae_biomass = ALGAE_BIOMASS*(self.section_rank*float(self.leaf.width)*float(self.leaf.length))/(0.5*self.leaf.section_num*(self.leaf.section_num+1))
                algae_attenuation_pct = (EPI_LIGHT_COEF*0.8)*exp(-1*algae_biomass/(EPI_LIGHT_COEF*3.63))
                
            if algae_biomass==0:
                algae_attenuation_pct =1
            
            #if algae_biomass < 0.00035:
                #QL = 1
            
            #print algae_biomass
            #print algae_attenuation_pct
            #print "#####"  
            
            #print algae_attenuation_pct
            ####################################
            #print "%s * %s * %s"%(time.surface_light, water_attenuation_pct, algae_attenuation_pct)
            #if time_available_light>846.15: P=Pmax=16.5*8.96*10^-4
            #else (if 140<available light<846.15) P=16.5*8.96*10^-4*availble light/(123.08+availible light)
            #    else (if availible light=140) P=13.82*8.96*10^-4
                     #else (if 9.4 <availible light <140) P= 13.82*8.96*10^-4*avalible light/(92.31+availble light)
                          #else P=0
            
             
            
            time_available_light=time.surface_light*water_attenuation_pct*algae_attenuation_pct   #uE/m^2.s

            if time_available_light>=LIGHT_SAT: 
                gross_photo_rate=PMAX #mgCO2/(m^2*s)
            elif IC<time_available_light<LIGHT_SAT:
                gross_photo_rate=PMAX*time_available_light/(KM+time_available_light) #mgCO2/(m^2*s)
            else:
                gross_photo_rate= 0
            
            gross_photo = gross_photo_rate*60*15*(1*self.leaf.width)/10000*0.68  #mgCO2/(15min)-->mgDW
            sum_gross_photo = sum_gross_photo + gross_photo
            
            if algae_biomass > 1:
              if algae_biomass >= 4.5: # most epi
                 respiration = EPI_BIO_LOSS_COEF*0.15*5*gross_photo + 0.4* gross_photo + RESPIRATION*(1*self.leaf.width)/10000*0.68
                 #respiration = 0.25*algae_biomass*gross_photo + 0.4* gross_photo + RESPIRATION*(1*self.leaf.width)/10000*0.68
              elif 2 < algae_biomass <= 4.5:
                 respiration = EPI_BIO_LOSS_COEF*0.2*algae_biomass*gross_photo + 0.4* gross_photo + RESPIRATION*(1*self.leaf.width)/10000*0.68
              elif 1< algae_biomass <=2:
                 respiration = EPI_BIO_LOSS_COEF*0.4*algae_biomass*gross_photo + 0.4* gross_photo + RESPIRATION*(1*self.leaf.width)/10000*0.68
            else:   #less epi
              respiration = EPI_BIO_LOSS_COEF*0.3*algae_biomass*gross_photo + 0.4* gross_photo + RESPIRATION*(1*self.leaf.width)/10000*0.68
            #if self.section_rank <= 45:
              #if algae_biomass >= 0.2: # mg DW/cm^2
                    #respiration = (0.4* gross_photo + 4.15*0.2*gross_photo) + RESPIRATION*(1*self.leaf.width)/10000*0.68
              #elif 0.1<algae_biomass<0.2:
                    #respiration = (0.4* gross_photo + 2*algae_biomass*gross_photo) + RESPIRATION*(1*self.leaf.width)/10000*0.68
              #elif 1<algae_biomass<=0.1:
                    #respiration = (0.4* gross_photo + 12*algae_biomass*gross_photo) + RESPIRATION*(1*self.leaf.width)/10000*0.68
              #else:
                    #respiration = (0.28* gross_photo) + RESPIRATION*(1*self.leaf.width)/10000*0.68
            #else:
                #respiration = (0.4* gross_photo + 1*gross_photo) + RESPIRATION*(1*self.leaf.width)/10000*0.68 
            #respiration = (0.4* gross_photo + 0.6*algae_biomass*gross_photo) + RESPIRATION*(1*self.leaf.width)/10000*0.68 
            sum_respiration = sum_respiration + respiration
            net_photo = QL*(gross_photo - respiration) #mgCO2/(m^2*s)-->mgDW
            sum_net_photo = sum_net_photo + net_photo #1mgDW=1.467mgCO2~1.65mgCO2 #1mgC02=0.606~0.682mgDW
            #print "water_attenuation_pct %s"%water_attenuation_pct
            current_time = timezone.localtime(time.date_and_time).strftime("%Y-%m-%d %H:%M")
            
            #print_out = "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s"%(self, current_time,time.surface_light,
            #                        time_depth, time.water.depth, water_attenuation_pct, 
            #                        algae_biomass,algae_attenuation_pct,
            #                        time_available_light,
            #                        gross_photo_rate,net_photo,sum_net_photo)
            #with open("log/section.csv", "a") as myfile:
            #     myfile.write(print_out+'\n')
            #print print_out
            #print 'time: %s net_photo:%s time_availible_light: %s time_depth: %s'%(time.date_and_time, net_photo, time_availible_light, time_depth)
        #print "sum_net_photo: %s"%sum_net_photo
        self.gross_photo = sum_gross_photo
        self.respiration = sum_respiration
        self.net_photo = sum_net_photo
        #exit()
        return self.net_photo
            #|7 days sum net photo-real data |^2=r2  
            #outplot: x list all parameter and r^2 
            
    '''
    sum-net-photo*growth rate=new growth #(CO2)
    (WHOLE LEAVE)
    '''
    
    
    def save(self, *args, **kwargs):
        if self.id != None:
            self.setCalNetPhoto()
        super(Section, self).save(*args, **kwargs)
    
    def __unicode__(self):
        return "Plant %s: Leaf: %s Section: %s" % (self.leaf.plant.id, self.leaf.leaf_rank, self.section_rank)
    
    #./recreate.sh #(clean old data and input new data)
    #./createsuperuser.sh  #if I want to see database (website) 
    #python manage.py cal_section  # calculate
    #python manage.py continue # continue
    #https://macrophyte-model-yangx.c9users.io/admin/macrophyte/light/ #website
    #https://macrophyte-model-yangx.c9users.io/admin/macrophyte/section/
    #sudo service postgresql start
    #sudo service redis-server start
    #https://bitbucket.org/y_xu/macrophyte_model
    
    #./start.sh
    #./recreate.sh
    #./celery_run.sh
    # http://24.91.12.125/admin/
    #bitbucket.org
    #./sql/section_sum.sh (ps:Calculate section net photo) 
    #./sql/plant_epi.sh (ps:Calculate epi density-total blade)
    #  1.cd data -> 2.#./drop_create.sh  (remove old data) 
       #3.#./rollback.sh (Run project see if it is right) 
       #3.open a new terminal run#./sql/plant_epi.sh) 
       #4. find result in file(sql/plant_epi.csv)

    #cd sql-> bash section_sum.sh
    #pg_dump macrophyte | gzip > macrophyte.gz ( download .gz)