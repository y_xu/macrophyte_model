# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

# Create your models here.

class Experiment(models.Model):
    name = models.CharField(max_length=50)
    biomass_file = models.FileField(upload_to='biomass/', blank=True)
    env_file = models.FileField(upload_to='env/', blank=True)
    ql = models.FloatField(default=0.75)
    kd = models.FloatField(default=1.56, help_text='(1/m)')
    pmax = models.FloatField(default=0.12, help_text='(mg CO2 g/(m^2*s)') #0.077
    km = models.FloatField(default=123.08, help_text='(uE/s/m^2)') #123.08
    respiration = models.FloatField(default=2.57, help_text='mgCO2/(m^2*15min)')
    ic = models.FloatField(default=9.4, help_text='(uE/s/m^2)')
    light_sat = models.FloatField(default=846.15, help_text='(uE/s/m^2)')
    epi_light_coef = models.FloatField(default=1)
    epi_bio_loss_coef = models.FloatField(default=1)
    algae_biomass = models.FloatField(null=True, blank=True, help_text='You can use 4.5')
    
    
    section_csv_download = models.FileField(upload_to='download/', blank=True)
    leaf_csv_download = models.FileField(upload_to='download/', blank=True)
    plant_csv_download = models.FileField(upload_to='download/', blank=True)


    def section_csv_link(self):
        if self.section_csv_download:
            return "<a href='%s'>download</a>" % (self.section_csv_download.url,)
        else:
            return "No attachment"

    section_csv_link.allow_tags = True

    def save(self, *args, **kwargs):
        super(Experiment, self).save(*args, **kwargs)
    
    def __unicode__(self):
        return "%s" % self.id
    
