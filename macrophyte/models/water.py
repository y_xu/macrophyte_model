# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Water(models.Model):
    depth = models.FloatField()
    tide = models.FloatField(null=True)
    attenuation_kd = models.FloatField(default=1.56) 

    @property
    def setTide(self):
        return 1
    
    @property
    def getCalDepth(self):
        return 1
    
    @property
    def getCalAttenuationKd(self):
        return 1
    
    def save(self, *args, **kwargs):
        super(Water, self).save(*args, **kwargs)
    
    def __unicode__(self):
        return "depth: %s kd: %s" % (self.depth, self.attenuation_kd)
    