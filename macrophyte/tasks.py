from celery import shared_task
from macrophyte.models import Plant, Leaf, Algae, Experiment
from macrophyte.models import Water, Light, Section

import csv
from datetime import datetime
from django.core.files import File


from django.utils import timezone
from dateutil.tz import tzlocal
from django.core.files import File

localtimezone = tzlocal()

@shared_task
def import_experiment_from_setting(setting):
    biomass_file = setting.biomass_file
    env_file = setting.env_file
    setting_file = setting.setting_file
    with open(setting_file.path) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            experiment = Experiment.objects.create()
            experiment.name = experiment.id
            experiment.biomass_file = File(biomass_file, biomass_file.name)
            experiment.env_file = File(env_file, env_file.name)
            try:
               if row['ql'] not in [None, '', ' ']:
                  experiment.ql = row['ql']
            except:
               pass
            try:
               if row['kd'] not in [None, '', ' ']:
                  experiment.kd = row['kd']
            except:
               pass
            try:
               if row['pmax'] not in [None, '', ' ']:
                  experiment.pmax = row['pmax']
            except:
               pass
            try:
               if row['km'] not in [None, '', ' ']:
                  experiment.km = row['km']
            except:
               pass
            try:
               if row['respiration'] not in [None, '', ' ']:
                  experiment.respiration = row['respiration']
            except:
               pass
            try:             
               if row['ic'] not in [None, '', ' ']:
                  experiment.ic = row['ic']
            except:
               pass
            try:
               if row['light_sat'] not in [None, '', ' ']:
                  experiment.light_sat = row['light_sat']
            except:
               pass
            try:
               if row['epi_light_coef'] not in [None, '', ' ']:
                  experiment.epi_light_coef = row['epi_light_coef']
            except:
               pass
            try:
               if row['epi_bio_loss_coef'] not in [None, '', ' ']:
                  experiment.epi_bio_loss_coef = row['epi_bio_loss_coef']
            except:
               pass
            try:
               if row['algae_biomass'] not in [None, '', ' ']:
                  experiment.algae_biomass = row['algae_biomass']
            except:
               pass
            experiment.save()
    return "Import Setting Done"


@shared_task
def import_data(experiment):
        biomass_file = experiment.biomass_file.path
        with open(biomass_file) as csvfile:
            reader = csv.DictReader(csvfile)
            #experiment, created = Experiment.objects.get_or_create(id=0)
            plant = Plant.objects.create(experiment=experiment)
            for row in reader:
                
                algae = Algae.objects.create(algae_biomass=row['epi_dw'])
                leaf, created = Leaf.objects.get_or_create(plant=plant, leaf_rank=row['leaf_rank'])
                                        
                leaf.algae=algae
                leaf.age=row['leaf_age']
                leaf.length=row['leaf_length']
                leaf.width=row['leaf_width']
                leaf.real_growth = row['nb_dw']
                leaf.save()
                plant.real_growth = float(plant.real_growth)+float(leaf.real_growth)
                #experiment, created = Experiment.objects.get_or_create(id=row['experiment_id'])
                #plant.experiment = experiment
                plant.save()
                if row['leaf_rank'] == '1' or row['leaf_rank'] == 1:
                    #experiment, created = Experiment.objects.get_or_create(id=0)
                    plant = Plant.objects.create(experiment=experiment)
                print "Save:"
                #print(row['leaf_rank'], row['leaf_age'], row['leaf_length'])
            plant.delete()
        #experiment, created = Experiment.objects.get_or_create(id=0)
        #experiment.delete()
        
        Light.objects.filter(experiment=experiment).delete()
        env_file = experiment.env_file.path
        with open(env_file) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                #experiment, created = Experiment.objects.get_or_create(id=row['experiment_id'])
                
                #date_and_time = datetime.strptime(row['time'], '%m/%d/%Y %H:%M').replace(tzinfo=localtimezone)
                date_and_time = datetime.strptime(row['time'], '%b %d %Y  %I:%M %p').replace(tzinfo=localtimezone)
                water = Water.objects.create(depth=float(row['water_level']))
                light, created = Light.objects.get_or_create(experiment=experiment,
                                                            date_and_time=date_and_time)
                light.surface_light=row['field_light']
                light.water=water
                light.save()
                print date_and_time, row['water_level'], row['field_light']
        return "Import Success"

'''
@shared_task
def import_data(obj):
        biomass_file = obj.biomass_file.path
        with open(biomass_file) as csvfile:
            reader = csv.DictReader(csvfile)
            experiment, created = Experiment.objects.get_or_create(id=0)
            plant = Plant.objects.create(experiment=experiment)
            for row in reader:
                
                algae = Algae.objects.create(algae_biomass=row['epi_dw'])
                leaf = Leaf.objects.create(plant=plant, 
                                        algae=algae,
                                        leaf_rank=row['leaf_rank'],
                                        age=row['leaf_age'],
                                        length=row['leaf_length'],
                                        width=row['leaf_width'],
                                        real_growth = row['nb_dw'])
                plant.real_growth = float(plant.real_growth)+float(leaf.real_growth)
                experiment, created = Experiment.objects.get_or_create(id=row['experiment_id'])
                plant.experiment = experiment
                plant.save()
                if row['leaf_rank'] == '1' or row['leaf_rank'] == 1:
                    experiment, created = Experiment.objects.get_or_create(id=0)
                    plant = Plant.objects.create(experiment=experiment)
                print "Save:"
                #print(row['leaf_rank'], row['leaf_age'], row['leaf_length'])
            plant.delete()
        experiment, created = Experiment.objects.get_or_create(id=0)
        experiment.delete()
        
        env_file = obj.env_file.path
        with open(env_file) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                experiment, created = Experiment.objects.get_or_create(id=row['experiment_id'])
                
                #date_and_time = datetime.strptime(row['time'], '%m/%d/%Y %H:%M').replace(tzinfo=localtimezone)
                date_and_time = datetime.strptime(row['time'], '%b %d %Y  %I:%M %p').replace(tzinfo=localtimezone)
                water = Water.objects.create(depth=float(row['water_level']))
                light, created = Light.objects.get_or_create(experiment=experiment,
                                                            surface_light=row['field_light'],
                                                            date_and_time=date_and_time,
                                                            water=water)
                print date_and_time, row['water_level'], row['field_light']
        return "Import Success"
''' 
@shared_task
def run_data(experiment):
            light = Light.objects.filter(experiment=experiment)
            for plant in Plant.objects.filter(experiment=experiment).order_by('id'):
                #for leaf in Leaf.objects.filter(leaf_rank=5).order_by('-leaf_rank'):
                for leaf in Leaf.objects.filter(plant=plant).order_by('-leaf_rank'):
                    for rank in xrange(leaf.section_num):
                        section_rank = rank + 1
                        section, created = Section.objects.get_or_create(leaf=leaf, section_rank=section_rank)
                        section.light=light
                        section.save()
                        print "%s net_photo: %s"%(section, section.net_photo)
                    leaf.setNetPhoto()
                    leaf.setGrossPhotoProduct()
                    leaf.setRespiration()
                    leaf.setSelfUsedNetPhoto()
                    leaf.save()
                    print "%s saved"%leaf
                #exit()
                plant.setNetPhoto()
                plant.setGrossPhotoProduct()
                plant.setRespiration()
                plant.setPredictedGrowth()
                plant.setNewLeaf()
                plant.save()
                print "%s saved"%plant

@shared_task
def continue_run_data(experiment):
            light = Light.objects.filter(experiment=experiment)
            for plant in Plant.objects.filter(experiment=experiment).order_by('id'):
                #for leaf in Leaf.objects.filter(leaf_rank=5).order_by('-leaf_rank'):
                for leaf in Leaf.objects.filter(plant=plant).order_by('-leaf_rank'):
                    for rank in xrange(leaf.section_num):
                        section_rank = rank + 1
                        section, created = Section.objects.get_or_create(leaf=leaf, section_rank=section_rank)
                        if created or section.net_photo==None:
                           section.light=light
                           section.save()
                        
                        print "%s net_photo: %s"%(section, section.net_photo)
                    leaf.setNetPhoto()
                    leaf.save()
                    print "%s saved"%leaf
                #exit()
                plant.setNetPhoto()
                plant.save()
                print "%s saved"%plant
                
@shared_task
def section_csv_download(experiment):
    file_name = '%s_%s.csv'%('section', datetime.now().strftime("%Y-%m-%dT%H%M%S"))
    with open(file_name, "w+") as f:
        f.write('id,gross_photo,respiration,net_photo\n')
        for section in Section.objects.filter(leaf__plant__experiment=experiment):
            name = "Plant %s: Leaf: %s Section: %s" % (section.leaf.plant.id, section.leaf.leaf_rank, section.section_rank)
            print_out = '%s,%s,%s,%s\n'%(name, section.gross_photo, section.respiration, section.net_photo)
            f.write(print_out)
        wrapped_file = File(f)
        experiment.section_csv_download = wrapped_file
        experiment.save()
    return "Done"
    
@shared_task
def leaf_csv_download(experiment):
    file_name = '%s_%s.csv'%('leaf', datetime.now().strftime("%Y-%m-%dT%H:%M:%S"))
    pass
    
    
@shared_task
def plant_csv_download(experiment):
    file_name = '%s_%s.csv'%('plant', datetime.now().strftime("%Y-%m-%dT%H:%M:%S"))
    pass
    
