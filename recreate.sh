#!/bin/bash
dropdb macrophyte
createdb macrophyte
rm macrophyte/migrations/000*
rm biomass/*
rm env/*
python manage.py makemigrations
python manage.py migrate
psql -d macrophyte -c "drop table celery_taskmeta;"
psql -d macrophyte -c "drop table celery_tasksetmeta;"
python manage.py migrate
python manage.py createsuperuser
