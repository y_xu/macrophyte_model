gunzip -c macrophyte.gz | psql -d macrophyte -U ubuntu
psql -d macrophyte -c "ALTER TABLE macrophyte_experiment ADD COLUMN ql double precision;"
psql -d macrophyte -c "ALTER TABLE macrophyte_experiment ADD COLUMN ic double precision;"
psql -d macrophyte -c "ALTER TABLE macrophyte_experiment ADD COLUMN light_sat double precision;"
psql -d macrophyte -c "ALTER TABLE macrophyte_experiment ADD COLUMN epi_light_coef double precision;"
psql -d macrophyte -c "ALTER TABLE macrophyte_experiment ADD COLUMN epi_bio_loss_coef double precision;"